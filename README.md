# Neo4jBase.jl

Julia library for interacting with [Neo4j](https://neo4j.com/) databases using the [HTTP API](https://neo4j.com/docs/http-api/current/actions/#http-api-actions).

Supports only basic HTTP methods for now, but is functional. Written since the two other Julia Neo4j packages do not work [Neo4jBolt.jl](https://github.com/virtualgraham/Neo4jBolt.jl/issues) and [Neo4j.jl](https://github.com/glesica/Neo4j.jl).

More functionality and documentation may come later, PRs welcome.
